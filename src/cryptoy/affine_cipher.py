from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement affine


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, en sortie on doit avoir une liste result tel que result[i] == (a * i + b) % n
    return [(a * i + b) % n for i in range(n)]


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, pour cela on appelle perm = compute_permutation(a, b, n) et on calcule la permutation inverse
    # result qui est telle que: perm[i] == j implique result[j] == i
    perm: list[int] = compute_permutation(a, b, n)
    result: list[int] = [0] * len(perm)
    for i, j in enumerate(perm):
        result[j] = i
    return result


def encrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_permutation, str_to_unicodes et unicodes_to_str
    n: int = 0x110000
    unicodes: list[int] = str_to_unicodes(msg)
    result: list[int] = [compute_permutation(a, b, n)[u] for u in unicodes]
    return unicodes_to_str(result)


def encrypt_optimized(msg: str, a: int, b: int) -> str:
    # A implémenter, sans utiliser compute_permutation
    unicodes: list[int] = str_to_unicodes(msg)
    result: list[int] = []
    N: int = 0x110000
    for i in range(a):
        if gcd(i, N) == 1:
            result = [(a * u + b) % N for u in unicodes]
            break
    return unicodes_to_str(result)


def decrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_inverse_permutation, str_to_unicodes et unicodes_to_str
    n: int = 0x110000
    unicodes: list[int] = str_to_unicodes(msg)
    result: list[int] = [compute_inverse_permutation(a, b, n)[u] for u in unicodes]

    return unicodes_to_str(result)


def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    # A implémenter, sans utiliser compute_inverse_permutation
    # On suppose que a_inverse a été précalculé en utilisant compute_affine_key_inverse, et passé
    # a la fonction
    N: int = 0x110000
    unicodes: list[int] = str_to_unicodes(msg)
    result: list[int] = [(a_inverse * (u - b)) % N for u in unicodes]

    return unicodes_to_str(result)


def compute_affine_keys(n: int) -> list[int]:
    # A implémenter, doit calculer l'ensemble des nombre a entre 1 et n tel que gcd(a, n) == 1
    # c'est à dire les nombres premiers avec n
    return [i for i in range(n) if gcd(i, n) == 1]


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    # Trouver a_1 dans affine_keys tel que a * a_1 % N == 1 et le renvoyer
    # Placer le code ici (une boucle)
    for key in affine_keys:
        if (a * key) % n == 1:
            return key

    # Si a_1 n'existe pas, alors a n'a pas d'inverse, on lance une erreur:
    raise RuntimeError(f"{a} has no inverse")


def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg et b == 58
    b: int = 58
    for i in range(0x110000):
        decrypted: str = decrypt(s, i, b)
        if 'bombe' in decrypted:
            return (decrypted, (i, b))
    # Placer le code ici

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg
    for a in range(0x110000):
        for b in range(1, 10000):
            inv = compute_inverse_permutation(a, b, 0x110000)[a]
            decrypted = decrypt_optimized(s, inv, b)
            if 'bombe' in decrypted:
                return (decrypted, (a, b))
    # Placer le code ici
    raise RuntimeError("Failed to attack")
